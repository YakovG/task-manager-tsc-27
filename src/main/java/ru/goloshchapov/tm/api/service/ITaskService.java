package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull Task add (String userId, String name, String description);

}

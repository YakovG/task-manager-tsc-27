package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IPropertyService;

public interface ServiceLocator {

    @NotNull ICommandService getCommandService();

    @NotNull ITaskService getTaskService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull IUserService getUserService();

    @NotNull IAuthService getAuthService();

    @NotNull IPropertyService getPropertyService();
}

package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIdViewCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-view-by-id";

    @NotNull public static final String DESCRIPTION = "Show project by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }
}

package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;

public final class TestDataCreateCommand extends AbstractCommand {

    @NotNull public static final String NAME = "create-test-data";

    @NotNull public static final String DESCRIPTION = "Create test dataset";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("CREATING TEST DATASET");
        serviceLocator.getProjectTaskService().createTestData();
    }
}

package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIdRemoveAllCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-remove-all-by-project-id";

    @NotNull public static final String DESCRIPTION = "Remove all tasks by project id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().removeAllByProjectId(userId, projectId);
        if (tasks == null) throw new TaskNotFoundException();
        else tasks.clear();
    }
}

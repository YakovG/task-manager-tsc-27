package ru.goloshchapov.tm.comparator;

import ru.goloshchapov.tm.entity.IHaveCreated;

import java.util.Comparator;

public final class ComparatorByCreated implements Comparator<IHaveCreated> {

    private static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    private ComparatorByCreated() {
    }

    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHaveCreated o1, final IHaveCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}

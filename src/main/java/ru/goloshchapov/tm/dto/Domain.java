package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.model.User;

import java.io.Serializable;
import java.util.List;

public final class Domain implements Serializable {

    @Setter
    @Getter
    @Nullable
    private List<Project> projects;

    @Setter
    @Getter
    @Nullable
    private List<Task> tasks;

    @Setter
    @Getter
    @Nullable
    private List<User> users;

}

package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
